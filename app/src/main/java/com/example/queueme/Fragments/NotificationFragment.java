package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.NotificationActivity;
import com.example.queueme.Adapters.NotificationAdapter;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.Notifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    ImageView ivBack;
    RecyclerView recyclerView;
    ArrayList<Notifications> notifications;
    FragmentManager fragmentManager;
    String current_user_id;
    int notifyID = 1;
    View view;


    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_notification, container, false);
        notificationListApi();

//        SessionManager sessionManager = new SessionManager(NotificationActivity.this);
//        HashMap<String, String> user = sessionManager.getUserDetails();
//        if (user != null) {
//            current_user_id = user.get(SessionManager.USER_IDD);
//        }

        SharedPreferences mPrefs=getActivity().getSharedPreferences("NotificationBadgeCount", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=mPrefs.edit();
        editor.putInt("count",0);
        editor.apply();

        init();
        return view;
    }
    private void init() {

        recyclerView = view.findViewById(R.id.notification_rv);

    }
    private void notificationListApi() {

        current_user_id= Utilities.getString(getActivity(),"id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    notifications = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("success");
                    if (status.equals("1")) {

                        progressdialog.dismiss();
                        JSONArray jsonArray = object.getJSONArray("result");

                        for (int i = 0; i< jsonArray.length(); i++){

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String id = jsonObject.getString("id");
                            String message = jsonObject.getString("message");
                            String userid = jsonObject.getString("user_id");
                            String created_at = jsonObject.getString("created_at");
                            String shopid = jsonObject.getString("shop_id");


                            notifications.add(new Notifications(id,userid,shopid,message,created_at));




                        }

                        NotificationAdapter notificationAdapter = new NotificationAdapter(notifications,getActivity(),fragmentManager);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(notificationAdapter);


                    } else {
                        progressdialog.dismiss();
                        if (getActivity() != null) {
                            String error = object.getString("message");
                            Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//
                params.put("do", "notifications");
                params.put("apikey", "mtechapi12345");
                params.put("user_id",current_user_id );
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }
}
