package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Activities.LandingActivity;
import com.example.queueme.Adapters.AdminAdapter;
import com.example.queueme.Adapters.ClerkAdapter;
import com.example.queueme.Models.AdminHomeModel;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class AdminHomeFragment extends Fragment implements AdminAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    String id, error;
    private View view;
    private RecyclerView rvHome;
    private AdminAdapter pAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager mLayoutManager;
    ArrayList<String> fileToUplado = new ArrayList<String>();
    private ArrayList<AdminHomeModel> adminHomeModels;

    public AdminHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_admin_home, container, false);
        initViews();
        getAdminShops();
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    private void initViews() {
        rvHome = view.findViewById(R.id.rvAdminHome);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getAdminShops() {

        id = Utilities.getString(getContext(), "id");
//            chanel_Categ_id = Utilities.getInt(getContext(), "Ch_cat_id");
//
//    final ProgressDialog progressdialog = new ProgressDialog(getActivity());
//    progressdialog.setIndeterminate(true);
//    progressdialog.setMessage("Please wait..");
//    progressdialog.setCancelable(false);
//    progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));
//
//    progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/admin_shops", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    adminHomeModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);


                            String ShopId = jsonObject.getString("id");
//                        String shopCode = jsonObject.getString("shop_code");
                            String AdminshopName = jsonObject.getString("shop_name");
                            String noOfQ = jsonObject.getString("queue_count");
                            String storAdminID = jsonObject.getString("store_admin_id");
                            String shop_code = jsonObject.getString("shop_code");
                            String statuss = jsonObject.getString("status");
                            String image = jsonObject.getString("image");
//                            JSONArray fileToUpload = jsonObject.getJSONArray("filesToUpload");
//                            String files= fileToUpload.toString();
//                            for (int j = 0; j < fileToUpload.length(); j++) {
//                                JSONObject jsonObject1 = fileToUpload.getJSONObject(0);
//
//                                String name = jsonObject1.getString("name");
//                                String type = jsonObject1.getString("type");
//                                String value = jsonObject1.getString("value");
//                                String image1 = jsonObject1.getString("image");
//                                String width = jsonObject1.getString("width");
//                                String height = jsonObject1.getString("height");
//
//                            }
                            String openingDay = jsonObject.getString("opening_day");
                            String closingDay = jsonObject.getString("closing_day");
                            String closingTime = jsonObject.getString("closing_time");
                            String openingTime = jsonObject.getString("opening_time");
//                            String sortOrder = jsonObject.getString("sortOrder");

//                            Utilities.saveString(getContext(), "AdminshopId", ShopId);
////                        Utilities.saveString(getContext(), "shopCode", shopCode);
//                            Utilities.saveString(getContext(), "shopName", AdminshopName);
//                            Utilities.saveString(getContext(), "noOfQ", noOfQ);
//
//                            Utilities.saveString(getContext(), "openDay", openingDay);
//                            Utilities.saveString(getContext(), "closeDay", closingDay);
//                            Utilities.saveString(getContext(), "closeTime", closingTime);
//                            Utilities.saveString(getContext(), "openTime", openingTime);

                            adminHomeModels.add(new AdminHomeModel(ShopId, AdminshopName, noOfQ, storAdminID, shop_code, closingTime, openingTime, closingDay, openingDay, statuss, image));

                        }
                        pAdapter = new AdminAdapter(getContext(), adminHomeModels, AdminHomeFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvHome.setLayoutManager(linearLayoutManager);
                        rvHome.setAdapter(pAdapter);


                    } else {
                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//       Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//            Utilities.hideProgressDialog();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("store_admin_id", id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    @Override
    public void onItemClick(int pos) throws JSONException {


        String shopId = adminHomeModels.get(pos).getAdminShopId();
        Utilities.saveString(getContext(), "AshopId", shopId);

        String shop_code = adminHomeModels.get(pos).getShopCode();
        Utilities.saveString(getContext(), "shop_code", shop_code);

        String openDay = adminHomeModels.get(pos).getOpenDay();
        Utilities.saveString(getContext(), "openDay", openDay);
        String closeDay = adminHomeModels.get(pos).getCloseDay();
        Utilities.saveString(getContext(), "closeDay", closeDay);
        String closeTime = adminHomeModels.get(pos).getCloseTime();
        Utilities.saveString(getContext(), "closeTime", closeTime);
        String openTime = adminHomeModels.get(pos).getOpenTime();
        Utilities.saveString(getContext(), "openTime", openTime);

        String shopName = adminHomeModels.get(pos).getAdminShopName();
        Utilities.saveString(getContext(), "Asshop_Name", shopName);

        String store_admin = adminHomeModels.get(pos).getStoreAdminId();
        Utilities.saveString(getContext(), "store_admnId", store_admin);
        String statusss = adminHomeModels.get(pos).getStatus();
        Utilities.saveString(getContext(), "statusss", statusss);
        String image = adminHomeModels.get(pos).getImage();
        Utilities.saveString(getContext(), "image", image);
//        String files = adminHomeModels.get(pos).getFiles();
//        Utilities.saveString(getContext(), "files", files);
        ((AdminActivity) getActivity()).navController.navigate(R.id.action_adminHomeFragment_to_adminStoreFragment);

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                onRefreshfrag();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }, 1000);

    }

    public void onRefreshfrag() {

        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }
}
