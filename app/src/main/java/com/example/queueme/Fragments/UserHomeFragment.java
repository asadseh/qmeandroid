package com.example.queueme.Fragments;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.MainActivity;
import com.example.queueme.Activities.ScannedBarcodeActivity;
import com.example.queueme.Activities.SplashOrignalActivity;
import com.example.queueme.Activities.UserActivity;
import com.example.queueme.Adapters.UserAdapter;
import com.example.queueme.Models.UserHomeModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddShopDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class UserHomeFragment extends Fragment implements UserAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    String id, error;
    private View view;
    ImageView ivAdd;
    private RecyclerView rvUserHome;
    private UserAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<UserHomeModel> userHomeModels;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public UserHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_home, container, false);
        initViews();
        getUserShops();
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    private void initViews() {
        rvUserHome = view.findViewById(R.id.rvUserHome);
        ivAdd = view.findViewById(R.id.ivAdd);
        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFreeTrialDialog();
//                startActivity(new Intent(getActivity(), ScannedBarcodeActivity.class));

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void showFreeTrialDialog() {
        DialogFragment newFragment = AddShopDialog.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }


    private void getUserShops() {

        id = Utilities.getString(getContext(), "id");

//        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
//        progressdialog.setIndeterminate(true);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));
//
//        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/user_shops", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userHomeModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);


                            String shop_id = jsonObject.getString("shop_id");
//                        String shopCode = jsonObject.getString("shop_code");
                            String userShopName = jsonObject.getString("shop_name");
                            String totalNoOfQ = jsonObject.getString("queue_count");
                            String shop_code = jsonObject.getString("shop_code");

                            String follow_id = jsonObject.getString("follow_id");

                            String openingDay = jsonObject.getString("opening_day");
                            String closingDay = jsonObject.getString("closing_day");
                            String closingTime = jsonObject.getString("closing_time");
                            String openingTime = jsonObject.getString("opening_time");
                            String image = jsonObject.getString("image");

//                            Utilities.saveString(getContext(), "shopId", shop_id);
////                        Utilities.saveString(getContext(), "shopCode", shopCode);
//                            Utilities.saveString(getContext(), "shopName", userShopName);
//                            Utilities.saveString(getContext(), "noOfQ", totalNoOfQ);
//                            Utilities.saveString(getContext(), "shop_code", shop_code);
//
//                            Utilities.saveString(getContext(), "openDay", openingDay);
//                            Utilities.saveString(getContext(), "closeDay", closingDay);
//                            Utilities.saveString(getContext(), "closeTime", closingTime);
//                            Utilities.saveString(getContext(), "openTime", openingTime);

                            userHomeModels.add(new UserHomeModel(shop_id, userShopName, totalNoOfQ, shop_code, follow_id, closingTime, openingTime, closingDay, openingDay, image));

                        }
                        pAdapter = new UserAdapter(getContext(), userHomeModels, UserHomeFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvUserHome.setLayoutManager(linearLayoutManager);
                        rvUserHome.setAdapter(pAdapter);


                    } else {

                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    @Override
    public void onItemClick(int pos) throws JSONException {
        String shopName = userHomeModels.get(pos).getUserShopName();
        Utilities.saveString(getContext(), "UserShopName", shopName);
        String shopCode = userHomeModels.get(pos).getShop_code();
        Utilities.saveString(getContext(), "shop_code1", shopCode);
        String shopId = userHomeModels.get(pos).getUserShopId();
        Utilities.saveString(getContext(), "shop_id", shopId);
        String follow_id = userHomeModels.get(pos).getFollowId();
        Utilities.saveString(getContext(), "follows_id", follow_id);

        String openTime = userHomeModels.get(pos).getOpenTime();
        Utilities.saveString(getContext(), "openTime", openTime);
        String clsoeTime = userHomeModels.get(pos).getCloseTime();
        Utilities.saveString(getContext(), "clsoeTime", clsoeTime);
        String image = userHomeModels.get(pos).getImage();
        Utilities.saveString(getContext(), "shop_image", image);

        ((UserActivity) getActivity()).navController.navigate(R.id.action_userHomeFragment_to_userHomeStoreFragment);


    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                onRefreshfrag();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }, 1000);

    }

    public void onRefreshfrag() {
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }
//    private void showButtons() {
//        llButton.setVisibility(View.VISIBLE);
//        Animation slideAnimation = AnimationUtils.loadAnimation(getActivity(),
//                R.anim.slide_left_to_right_animation);
//        llButton.startAnimation(slideAnimation);
//    }

}
