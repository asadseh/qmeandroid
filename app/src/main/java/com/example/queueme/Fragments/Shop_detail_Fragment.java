package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.ShopDetailAdapter;
import com.example.queueme.Models.ShopDetailModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.EditDayDialog;
import com.example.queueme.dialogs.EditTimesDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class Shop_detail_Fragment extends Fragment {
    String setDayString;
    private View view;
    private TextView tvDayOpen, tvDayClose, tvTimeOpen, tvTimeClose, tvShopName;
    RecyclerView rv_shop_detail;
    ImageView back_img, ivProfile;
    LinearLayout edit_day, ivEditTime;
    private ShopDetailAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<ShopDetailModel> shopDetailModels;
    String shopid, clerkId, openingDay, openingTime, closingDay, closingTime, shopName, error;
    String shopimage = "";

    public Shop_detail_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop_detail_, container, false);
        initViews();
        clickViews();
        getClerkQApi();
        return view;
    }

    private void initViews() {
        tvDayOpen = view.findViewById(R.id.tvDayOpen);
        tvDayClose = view.findViewById(R.id.tvDayClose);
        tvTimeOpen = view.findViewById(R.id.tvTimeOpen);
        tvTimeClose = view.findViewById(R.id.tvTimeClose);
        tvShopName = view.findViewById(R.id.tvShopName);
        back_img = view.findViewById(R.id.back_img);
        back_img.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_shop_detail_Fragment_to_homeFragment));
        rv_shop_detail = view.findViewById(R.id.rv_shop_detail);
        edit_day = view.findViewById(R.id.edit_day);
        ivEditTime = view.findViewById(R.id.ivEditTime);
        ivProfile = view.findViewById(R.id.ivProfile);
        shopimage = Utilities.getString(getContext(), "image");
        Picasso.with(getActivity()).load(Server.BASE_URL_PHOTO + shopimage).into(ivProfile);
//        setDayString=new String();
//        setDayString= EditDayDialog.daySett;
        shopName = Utilities.getString(getActivity(), "shopNames");
        openingDay = Utilities.getString(getActivity(), "opensDay");
        closingDay = Utilities.getString(getActivity(), "closeDay");
        closingTime = Utilities.getString(getActivity(), "clsoeTime");
        openingTime = Utilities.getString(getActivity(), "openTime");

        tvDayClose.setText(closingDay);
        tvDayOpen.setText(openingDay);
        tvTimeClose.setText(closingTime);
        tvTimeOpen.setText(openingTime);
        tvShopName.setText(shopName);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void clickViews() {
        edit_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFreeTrialDialog();
            }
        });
        ivEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditTimeDialog();
            }
        });
    }

//    private void setPoemsName() {
//        shopDetailModels = new ArrayList<>();
//        for (int i = 1; i <= 10; i++) {
//            ShopDetailModel poem = new ShopDetailModel();
//            poem.setShopName(" Shop" + i);
//            poem.setNoOfQ(""+i);
//            poem.setPages(10);
//            shopDetailModels.add(poem);
//        }
//    }
//
//    private void initAdapter() {
//
//        setPoemsName();
//        pAdapter = new ShopDetailAdapter(getActivity(), shopDetailModels);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//        rv_shop_detail.setLayoutManager(linearLayoutManager);
//        rv_shop_detail.setAdapter(pAdapter);
//    }

    private void showFreeTrialDialog() {
        DialogFragment newFragment = EditDayDialog.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }

    private void showEditTimeDialog() {
        DialogFragment newFragment = EditTimesDialog.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }

    private void getClerkQApi() {

        shopid = Utilities.getString(getContext(), "shopsId");
        clerkId = Utilities.getString(getContext(), "id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/clerk_queue_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    shopDetailModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            String Qid = jsonObject.getString("id");
                            String name = jsonObject.getString("queue_name");
                            String noOfPerson = jsonObject.getString("person");
//                            String shop_name = jsonObject.getString("shop_name");
//                            String store_admin = jsonObject.getString("store_admin");
                            String opening_day = jsonObject.getString("opening_day");
                            String closing_day = jsonObject.getString("closing_day");
                            String shopId = jsonObject.getString("shop_id");
//                            String shop_code = jsonObject.getString("shop_code");

                            String closing_time = jsonObject.getString("closing_time");
                            String opening_time = jsonObject.getString("opening_time");
                            String data_status = jsonObject.getString("status");

                            Utilities.saveString(getContext(), "clerkQListId", Qid);

//                            Utilities.saveString(getContext(), "shop_name", shop_name);
//                            Utilities.saveString(getContext(), "store_admin", store_admin);
                            Utilities.saveString(getContext(), "opening_day", opening_day);
                            Utilities.saveString(getContext(), "closing_day", closing_day);
                            Utilities.saveString(getContext(), "shopId", shopId);
//                            Utilities.saveString(getContext(), "shop_code", shop_code);

//                            Utilities.saveString(getContext(), "name", name);
                            Utilities.saveString(getContext(), "noOfPerson", noOfPerson);

                            shopDetailModels.add(new ShopDetailModel(name, noOfPerson, Qid, opening_time, closing_time, data_status, shopId));

                        }
                        pAdapter = new ShopDetailAdapter(getContext(), shopDetailModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rv_shop_detail.setLayoutManager(linearLayoutManager);
                        rv_shop_detail.setAdapter(pAdapter);


                    } else {

                        if (status == 400) {
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_id", shopid);
                params.put("clerk_id", clerkId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
}
