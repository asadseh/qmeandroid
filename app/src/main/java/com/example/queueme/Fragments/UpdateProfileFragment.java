package com.example.queueme.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.flatdialoglibrary.dialog.FlatDialog;
import com.example.queueme.Activities.ImagePickerActivity;
import com.example.queueme.Activities.LoginActivity;
import com.example.queueme.Activities.Signup1Activity;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddShopDialog;
import com.example.queueme.dialogs.PassRequirdDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

import static com.android.volley.VolleyLog.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateProfileFragment extends Fragment {
    EditText etUpdateuserName, etUpdatePass, etUpdateEmail, etUpdatePhone;
    View view;
    Button bUpdate;
    ImageView ivProfile;
    String phoneToUpdate, nameToUpdate, passToUpdate, emailToUpdate = "", id, user_group, error, email, pass, name, phone, photo1;

    Boolean photoChange = false;

    String mImageFileLocation;
    Uri file;
    boolean imageFromCamera = false;
    Uri imageUri;
    boolean photo_chnage = false;
    Uri uri = null;
    private static final int pic_id = 123;
    Bitmap photo;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int MY_SOCKET_TIMEOUT_MS = 100000;
    Bitmap bitmap_img;
    Bitmap selected_img_bitmap;
    String picture_str;

    String imageEncoded;
    String input, check_img = "";

    public static final int REQUEST_IMAGE = 100;

    public UpdateProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_update_profile, container, false);
        etUpdateEmail = view.findViewById(R.id.etUpdateEmail);
        etUpdatePass = view.findViewById(R.id.etUpdatePass);
        etUpdateuserName = view.findViewById(R.id.etUser_name);
        etUpdatePhone = view.findViewById(R.id.etUpdatePhone);
        bUpdate = view.findViewById(R.id.bUpdate1);
        ivProfile = view.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onProfileImageClick();
            }
        });

        email = Utilities.getString(getActivity(), "email");
        pass = Utilities.getString(getActivity(), "pass");
        name = Utilities.getString(getActivity(), "userName");
        phone = Utilities.getString(getActivity(), "phone");
        photo1 = Utilities.getString(getActivity(), "photo");

        etUpdateEmail.setText(email);
        etUpdatePass.setText(pass);
        etUpdateuserName.setText(name);
        etUpdatePhone.setText(phone);
        Picasso.with(getActivity()).load(Server.BASE_URL_PHOTO + photo1).into(ivProfile);
        bUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String confirmationPass = Utilities.getString(getActivity(), "password");
                if (confirmationPass.equals("matched")) {
                    Toast.makeText(getActivity(), "change Profile", Toast.LENGTH_SHORT).show();
                    emailToUpdate = etUpdateEmail.getText().toString();
                    passToUpdate = etUpdatePass.getText().toString();
                    nameToUpdate = etUpdateuserName.getText().toString();
                    phoneToUpdate = etUpdatePhone.getText().toString();
                    editprofileApiiwithimage();

                } else {
                    showFreeTrialDialog();
                }

            }
        });
//        bUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                emailToUpdate = etUpdateEmail.getText().toString();
//                passToUpdate = etUpdatePass.getText().toString();
//                nameToUpdate = etUpdateuserName.getText().toString();
//                phoneToUpdate = etUpdatePhone.getText().toString();
//                if (!emailToUpdate.equals("")) {
//
//                    editprofileApiiwithimage();
//                } else {
//                    etUpdateEmail.setError("field requred");
//
//                }
//
//
//            }
//        });

        return view;
    }

    private void signUpApi() {
        if (!nameToUpdate.equals("")) {
            if (!emailToUpdate.equals("")) {
                if (!phoneToUpdate.equals("")) {
                    if (!passToUpdate.equals("")) {

                        editprofileApiiwithimage();

                    } else {
                        etUpdatePass.setError("Field Required");
                    }
                } else {
                    etUpdateEmail.setError("Field Required");
                }
            } else {
                etUpdatePhone.setError("Field Required");
            }
        } else {
            etUpdateuserName.setError("Field Required");
        }
    }

    private void editprofileApiiwithimage() {

        id = Utilities.getString(getActivity(), "id");
        user_group = Utilities.getString(getActivity(), "user_group");


        try {

            if (uri != null) {
                selected_img_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);


                Bitmap immagex = selected_img_bitmap;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                input = imageEncoded;
                input = input.replace("\n", "");
                input = input.trim();
                input = "data:image/jpeg;base64," + input;
                check_img = "yes";
            } else {
                check_img = "no";
                input = photo1;
            }
            //hit Api


            final ProgressDialog progressdialog = new ProgressDialog(getActivity());
            progressdialog.setMessage("Please wait..");
            progressdialog.setCancelable(false);
            progressdialog.show();

            final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/edit_profile", new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject object = new JSONObject(response);
                        int status = object.getInt("status");
                        if (status == 200) {

                            progressdialog.dismiss();
                            String message = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(message)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
                                    .show();


                            JSONObject jsonObject = object.getJSONObject("data");
                            photo1 = jsonObject.getString("image");

                            name = jsonObject.getString("name");
                            email = jsonObject.getString("email");
                            phone = jsonObject.getString("phone");
//                            pass = jsonObject.getString("password");

                            Utilities.saveString(getActivity(), "photo", photo1);
                            Utilities.saveString(getActivity(), "userName", name);
                            Utilities.saveString(getActivity(), "email", email);
                            Utilities.saveString(getActivity(), "phone", phone);
//                            Utilities.saveString(getActivity(), "pass", pass);

                        } else {
                            progressdialog.dismiss();
                            if (getActivity() != null) {
                                if (status == 400) {
                                    error = object.getString("message");
                                    Utilities.hideProgressDialog();
                                    new PrettyDialog(getActivity())
                                            .setTitle(error)
                                            .setIcon(R.drawable.pdlg_icon_info)
                                            .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                            .show();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressdialog.dismiss();
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (volleyError instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", String.valueOf(id));
                    params.put("name", nameToUpdate);
                    params.put("email", emailToUpdate);
                    params.put("phone", phoneToUpdate);
                    params.put("photo", input);
                    params.put("password", passToUpdate);

                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept", "application/json");
                    return params;
                }

            };

            RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                    25000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);


        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(ivProfile);
        ivProfile.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));
    }

    private void onProfileImageClick() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(getActivity(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                photoChange = true;
                uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                    // loading profile image from local cache
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // SignupFragment.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void showFreeTrialDialog() {
        PassRequirdDialog newFragment = PassRequirdDialog.newInstance(
                R.string.search_word_meaning_dialog_title);
        newFragment.show(getFragmentManager(), "dialog");

    }
}
