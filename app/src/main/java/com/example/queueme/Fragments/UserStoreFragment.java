package com.example.queueme.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.UnAssignedQAdapter;
import com.example.queueme.Adapters.UserStoreAdapter;
import com.example.queueme.Models.UnAssignedModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddQDialog;
import com.example.queueme.dialogs.AddShopDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class UserStoreFragment extends Fragment {
    Button bFollowReq;
    private View view;
    private TextView shopName;
    RecyclerView rvUserStore,rvUnAssigned;
    ImageView back_img;
    private UserStoreAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    ArrayList<UserStoreModel> userStoreModels;
    String shopid, user_id, shop_code,shpname,error;
    ArrayList<UnAssignedModel> unAssignedModels;
    private UnAssignedQAdapter uAdapter;

    public UserStoreFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_store, container, false);
        initViews();
        getunAssignedApi();
//        getUserQQApi();
        userStoreModels = new ArrayList<>();
        userStoreModels = AddShopDialog.userStoreModels;


        pAdapter = new UserStoreAdapter(getContext(), userStoreModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvUserStore.setLayoutManager(linearLayoutManager);
        rvUserStore.setAdapter(pAdapter);

        return view;
    }

    private void initViews() {
        rvUserStore = view.findViewById(R.id.rvUserStore);
        shopName = view.findViewById(R.id.shop_name);
        rvUnAssigned = view.findViewById(R.id.rvUnAssigned);
        back_img = view.findViewById(R.id.back_img);

        String shopname = Utilities.getString(getActivity(), "shopNameee");
//        shopName.setText(shopname);

//        back_img.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_userStoreFragment_to_userHomeFragment));
        final Button testButton = view.findViewById(R.id.bFollow);
        testButton.setTag(1);
        testButton.setText("Follow");

        testButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                final int status = (Integer) v.getTag();
                if (status == 1) {
                    shopid = userStoreModels.get(0).getShopId();
                    getFollowApi();
                    testButton.setText("UnFollow");
                    testButton.setTextColor(R.color.white);
                    testButton.setBackgroundColor(getResources().getColor(R.color.blue));
                    v.setTag(0); //pause
                } else {
                    getUnFollowApi();
//                    testButton.setText("Follow");
                    v.setTag(1); //pause
                    testButton.setClickable(false);
                }
            }

        });
//        String shopname =AddShopDialog.shop_name;



    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getFollowApi() {

        user_id = Utilities.getString(getActivity(), "id");
        shop_code = Utilities.getString(getActivity(), "editShopCode");


        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        String message = object.getString("message");

                        progressdialog.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    } else {
                        if (status == 0){
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "follow_shop");
                params.put("apikey", "mtechapi12345");
                params.put("user_id", user_id);
                params.put("shop_code", shop_code);
                params.put("shop_id", shopid);
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
    private void getUnFollowApi() {

        user_id = Utilities.getString(getActivity(), "id");
//        shop_code = Utilities.getString(getActivity(), "editShopCode");
//        shopid = Utilities.getString(getActivity(), "shop_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    userStoreModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        String message = object.getString("message");

                        progressdialog.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    } else {
                        if (status == 0){
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "unfollow");
                params.put("apikey", "mtechapi12345");
                params.put("user_id", user_id);
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
    private void getunAssignedApi() {

        shopid = Utilities.getString(getContext(), "shop_id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    unAssignedModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        final JSONArray objUser = object.getJSONArray("result");
//
                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            String name = jsonObject.getString("queue_name");
                            String shp_id = jsonObject.getString("shop");
//                            String shop_name = jsonObject.getString("shop_name");
                            String noOfQ = jsonObject.getString("number_of_persons");
//
                            unAssignedModels.add(new UnAssignedModel(name,noOfQ,shp_id));

                            progressdialog.dismiss();

//
                        }
                        uAdapter = new UnAssignedQAdapter(getContext(), unAssignedModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvUnAssigned.setLayoutManager(linearLayoutManager);
                        rvUnAssigned.setAdapter(uAdapter);


                    } else {
                        if (status == 0){
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error+"for unassigned queue")
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "unassigned_queue_list");
                params.put("apikey", "mtechapi12345");
                params.put("shop_id", shopid);
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
}
