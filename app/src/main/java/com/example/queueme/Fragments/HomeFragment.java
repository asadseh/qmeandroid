package com.example.queueme.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.LandingActivity;
import com.example.queueme.Activities.LoginActivity;
import com.example.queueme.Adapters.ClerkAdapter;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class HomeFragment extends Fragment implements ClerkAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {
    String id,error;
    Context context;
    private View view;
    private RecyclerView rvClerkHome;
    private ClerkAdapter pAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<ClerkModel> clerkModels;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews();
        getClerkShops();
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    private void initViews() {
        rvClerkHome = view.findViewById(R.id.rvClerkHome);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    private void getClerkShops() {

        id = Utilities.getString(getContext(), "id");
//            chanel_Categ_id = Utilities.getInt(getContext(), "Ch_cat_id");
//
//        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
//        progressdialog.setIndeterminate(true);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));
//
//        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/clerk_shops", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    clerkModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {

                        // JSONObject obj = object.getJSONObject("result");
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);


                            String shopid = jsonObject.getString("id");
                            String shopCode = jsonObject.getString("shop_code");
                            String shopName = jsonObject.getString("shop_name");
                            String noOfQ = jsonObject.getString("queue_count");
                            String storeAdminId = jsonObject.getString("store_admin_id");

                            String openingDay = jsonObject.getString("opening_day");
                            String closingDay = jsonObject.getString("closing_day");
                            String closingTime = jsonObject.getString("closing_time");
                            String openingTime = jsonObject.getString("opening_time");
                            String image = jsonObject.getString("image");

//                            Utilities.saveString(getContext(), "openDay", openingDay);
//                            Utilities.saveString(getContext(), "closeDay", closingDay);
//                            Utilities.saveString(getContext(), "closeTime", closingTime);
//                            Utilities.saveString(getContext(), "openTime", openingTime);

                            clerkModels.add(new ClerkModel(shopName, noOfQ, shopid, storeAdminId, shopCode,openingTime,closingTime,openingDay,closingDay,image));

                        }
                        pAdapter = new ClerkAdapter(getContext(), clerkModels, HomeFragment.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvClerkHome.setLayoutManager(linearLayoutManager);
                        rvClerkHome.setAdapter(pAdapter);


                    } else {

                        if (status == 400){
                            error = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("clerk_id", id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

    @Override
    public void onItemClick(int pos) {
        String shopId = clerkModels.get(pos).getShopId();
        Utilities.saveString(getActivity(), "shopsId", shopId);
        String Qid = clerkModels.get(pos).getShopId();
        Utilities.saveString(getActivity(), "Qid", shopId);

        String shopName = clerkModels.get(pos).getShopName();
        Utilities.saveString(getActivity(), "shopNames", shopName);

        String adminId = clerkModels.get(pos).getStore_admin_id();
        Utilities.saveString(getActivity(), "strAdmin_Id", adminId);

        String openTime = clerkModels.get(pos).getOpenTime();
        Utilities.saveString(getActivity(), "openTime", openTime);

        String clsoeTime = clerkModels.get(pos).getCloseTime();
        Utilities.saveString(getActivity(), "clsoeTime", clsoeTime);


        String openDay = clerkModels.get(pos).getOpenDay();
        Utilities.saveString(getActivity(), "opensDay", openDay);

        String clsoeDay = clerkModels.get(pos).getCloseDay();
        Utilities.saveString(getActivity(), "closeDay", clsoeDay);
        String image = clerkModels.get(pos).getImage();
        Utilities.saveString(getActivity(), "image", image);

        Utilities.saveString(getContext(), "shop_code", clerkModels.get(pos).getShop_code());
        ((LandingActivity) getActivity()).navController.navigate(R.id.action_homeFragment_to_shop_detail_Fragment);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                onRefreshfrag();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        },1000);

    }
    public void onRefreshfrag(){
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

    }
}
