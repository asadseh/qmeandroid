package com.example.queueme.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateShopFragment extends Fragment {
    View view;
    ImageView ivClose;
    TextView tvStartTime, tvEndTime, tvStartDay, tvEndDay, error;
    TimePicker picker;
    EditText etShopName, etShopCode;
    Button bSave;
    Spinner spStartDay, spEndDay;

    String shopid,shopCode,h,m, shopsName,  shopName, openDay, closeDay,store_adminId,endTime, startTime,startDay, endDay, shop_code,error1,image;

    public UpdateShopFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_update_shop, container, false);

        spStartDay = view.findViewById(R.id.spinner1);
        spEndDay = view.findViewById(R.id.spinner2);
        bSave = view.findViewById(R.id.bSave);

        etShopName = view.findViewById(R.id.etShopName);
        etShopCode = view.findViewById(R.id.etShopCode);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        error = view.findViewById(R.id.error);
        tvEndTime = view.findViewById(R.id.tvEndTime);

        endTime = Utilities.getString(getContext(), "closeTime");
        shop_code = Utilities.getString(getContext(), "shop_code");
        startTime = Utilities.getString(getContext(), "openTime");
        openDay = Utilities.getString(getContext(), "openDay");
        closeDay = Utilities.getString(getContext(), "closeDay");
        shopName = Utilities.getString(getContext(), "Asshop_Name");
        shopid = Utilities.getString(getContext(), "AshopId");
        store_adminId = Utilities.getString(getContext(), "id");
        image = Utilities.getString(getContext(), "image");

        etShopCode.setText(shop_code);
        etShopName.setText(shopName);
        tvStartTime.setText(startTime);
        tvEndTime.setText(endTime);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sDOWEEKS, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spStartDay.setAdapter(adapter);
        if (openDay != null) {
            int spinnerPosition = adapter.getPosition(openDay);
            spStartDay.setSelection(spinnerPosition);
        }
        spStartDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                startDay = spStartDay.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        String[] datos = getResources().getStringArray(R.array.eDOWEEKS);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, datos);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spEndDay.setAdapter(adaptador);
        if (closeDay != null) {
            int spinnerPosition = adapter.getPosition(closeDay);
            spEndDay.setSelection(spinnerPosition);
        }
        spEndDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                endDay = spEndDay.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shopCode = etShopCode.getText().toString();
                shopsName = etShopName.getText().toString();


//                shoppName = Utilities.getString(getContext(), "AdminShopName");
//                openDay = Utilities.getString(getContext(), "openDay");
//                closeDay = Utilities.getString(getContext(), "closeDay");
//                closingTime = Utilities.getString(getContext(), "closeTime");
//                openingTime = Utilities.getString(getContext(), "openTime");
//                shoppCode = Utilities.getString(getContext(), "shop_code");

//                String startsTime=startTime;








                if (!shop_code.isEmpty()) {
                    error.setError(null);
                    if (!shopName.isEmpty()) {
                        error.setError(null);
                        if (!TextUtils.isEmpty(startTime)) {
                            error.setError(null);
                            error.setText("");
                            if (!TextUtils.isEmpty(endTime)) {
                                error.setText("");
                                error.setError(null);

                                if (!startDay.equals("Opening Day")) {
                                    error.setError(null);
                                    error.setText("");
                                    if (!endDay.equals("Closing Day")) {
                                        error.setError(null);
                                        error.setText("");
                                        UpdateQ();
                                    } else {
//                                        tvEndDay.setError("Field Required");
                                        error.setError("Closing Day required");
                                        error.setText("Closing Day required");

                                    }
                                } else {
                                    error.setError("Opening Day required");
                                    error.setText("Opening Day required");
//                                    tvStartDay.setError("Field Required");

                                }
                            } else {
                                error.setError("Closing Hour Required");
                                error.setText("Closing Hour Required");

                            }
                        } else {
                            error.setText("Opening Hour Required");
                            error.setError("Opening Hour Required");

                        }
                    } else {
                        etShopName.setError("Field Required");
                        error.setText("No. of Queue Required");
                        error.setError("");
                    }
                } else {
                    etShopCode.setError("Field Required");
                    error.setText("Queue Name Required");
                    error.setError("");

                }







//                UpdateQ();

            }
        });
        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String h="";
                        String AMPM="";

                        if (hourOfDay>12){
                            AMPM="PM";
                        }
                        else if (hourOfDay<12){
                            AMPM="AM";

                        }
                        else if(hourOfDay==12){

                            AMPM="PM";
                        }
                        else if(hourOfDay==00){

                            AMPM="AM";
                        }
//                        if (hourOfDay==00){
//                             h=String.valueOf(hourOfDay+=12);
//
//                        }
//
                        if(hourOfDay<10){
                            h ="0"+String.valueOf(hourOfDay);
                        }else {

                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m ="0"+String.valueOf(minute);
                        }
                        else{
                            m=String.valueOf(minute);
                        }
                        startTime = h + ":" + m;
                        tvStartTime.setText(startTime);
                    }
                };

                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);

                // Whether show time in 24 hour format or not.
                boolean is24Hour = false;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), onTimeSetListener, hour, minute, is24Hour);
                timePickerDialog.setTitle("Select End Time.");

                timePickerDialog.show();
            }
        });

        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String AM_PM;

//                            AM_PM = "AM";
//                            tvEndTime.setText(hourOfDay);
//                        if (hourOfDay>12){
//                            h=String.valueOf(hourOfDay-=12);
//
//                        }else if (hourOfDay == 0) {
//                            h=String.valueOf(hourOfDay+=12);
//                        }
//                        if (hourOfDay==00){
//                            h=String.valueOf(hourOfDay+=12);
//
//                        }
                        if(hourOfDay<10){
                            h ="0"+String.valueOf(hourOfDay);
                        }else {

                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m ="0"+String.valueOf(minute);
                        }
                        else{
                            m=String.valueOf(minute);
                        }
                        endTime = h + ":" + m;
                        tvEndTime.setText(endTime);
                    }
                };

                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);

                // Whether show time in 24 hour format or not.
                boolean is24Hour = false;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), onTimeSetListener, hour, minute, is24Hour);
                timePickerDialog.setTitle("Select End Time.");

                timePickerDialog.show();
            }
        });


        return view;
    }

    private void UpdateQ() {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/shop_update", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();

                        progressdialog.dismiss();
//                        ((AdminActivity)getActivity()).navController.navigate(R.id.action_addShopDialog_to_userHomeStoreFragment);


                    } else {
                        if (status == 400){
                            error1 = object.getString("message");
                            Utilities.hideProgressDialog();
                            new PrettyDialog(getActivity())
                                    .setTitle(error1)
                                    .setIcon(R.drawable.pdlg_icon_info)
                                    .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                    .show();
                        }
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shopCode);
                params.put("name", shopsName);
                params.put("store_admin", store_adminId);
                params.put("image",image);
                params.put("opening_day", startDay);
                params.put("closing_day", endDay);
                params.put("opening_time", startTime);
                params.put("closing_time", endTime);
                params.put("status", "Active");
                params.put("shop_id", shopid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
}
