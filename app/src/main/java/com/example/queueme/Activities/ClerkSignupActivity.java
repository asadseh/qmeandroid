package com.example.queueme.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.android.volley.VolleyLog.TAG;

public class ClerkSignupActivity extends AppCompatActivity implements View.OnClickListener {

    private CircleImageView ivProfile;
    private TextView upload_img, login, terms_text;
    private EditText f_name, phone_no, email, password, c_password;
    private String photo, full_name, phone, u_email, u_pass, u_cPass, text;
    private Button signup_btn;
    private SpannableString ss;
    String imageEncoded;
    String input;
    Uri uri;
    boolean photo_chnage = false;
    Bitmap selected_img_bitmap;
    String date;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    public static final int REQUEST_IMAGE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clerk_signup);
        ImagePickerActivity.clearCache(ClerkSignupActivity.this);
        initViews();
        termsPolicy();


    }

    //initializing Views
    private void initViews() {
        ivProfile = findViewById(R.id.ivProfile);
        login = findViewById(R.id.login);
        f_name = findViewById(R.id.etName);
        phone_no = findViewById(R.id.etPhone);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.password);
        c_password = findViewById(R.id.etConfirmPass);

        terms_text = findViewById(R.id.terms_text);

        signup_btn = findViewById(R.id.signup_btn);

        ivProfile.setOnClickListener(this);
        signup_btn.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivProfile:
                onProfileImageClick();
                break;
            case R.id.login:
                startActivity(new Intent(ClerkSignupActivity.this, LoginActivity.class));
                break;
            case R.id.signup_btn:
                full_name = f_name.getText().toString().trim();
                phone = phone_no.getText().toString().trim();
                u_email = email.getText().toString().trim();
                u_pass = password.getText().toString().trim();
                u_cPass = c_password.getText().toString().trim();

                Utilities.saveString(ClerkSignupActivity.this, "userName", full_name);
                Utilities.saveString(ClerkSignupActivity.this, "phone", phone);
                signUpApi();
                break;
        }

    }

    private void signUpApi() {
        if (photo_chnage) {
            if (!full_name.isEmpty()) {
                if (!phone.isEmpty()) {
                    if (!u_email.isEmpty()) {
                        if (!u_pass.isEmpty()) {
                            if (!u_cPass.isEmpty()) {
                                if (u_pass.equals(u_cPass)) {
                                    SignupApiCall(full_name, u_email, u_pass, phone);
                                } else {
                                    c_password.setError("Password Not Matched");
                                }
                            } else {
                                c_password.setError("Field Required");
                            }
                        } else {
                            password.setError("Field Required");
                        }
                    } else {
                        email.setError("Field Required");
                    }
                } else {
                    phone_no.setError("Field Required");
                }
            } else {
                f_name.setError("Field Required");
            }
        } else {
            Utilities.makeToast(ClerkSignupActivity.this, "Please Upload Image to continue");
        }
    }

    //terms of Use and  Privacy Policy clickable spans here
    private void termsPolicy() {

        text = "By creating an account you agree to our Terms of Use and Privacy Policy";
        ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(ClerkSignupActivity.this, "Terms of Use", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(ClerkSignupActivity.this, "Privacy Policy", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan1, 40, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 57, 71, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        terms_text.setText(ss);
        terms_text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void SignupApiCall(final String full_name, final String email, final String password, final String phone) {
//
        try {
            selected_img_bitmap = MediaStore.Images.Media.getBitmap(ClerkSignupActivity.this.getContentResolver(), uri);

            Bitmap immagex = selected_img_bitmap;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);

//            Bitmap immagex = selected_img_bitmap;
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//            byte[] b = baos.toByteArray();
//            imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

            input = imageEncoded;
            input = input.replace("\n", "");
            input = input.trim();

            final ProgressDialog progressdialog;
            progressdialog = new ProgressDialog(ClerkSignupActivity.this);
            progressdialog.setMessage("Please wait..");
            progressdialog.setCancelable(false);
            progressdialog.show();

            final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/signup_clerk", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject object = new JSONObject(response);
                        int status = object.getInt("status");
                        if (status == 200) {
                            progressdialog.dismiss();
                            String message = object.getString("message");
                            Toast.makeText(ClerkSignupActivity.this, message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ClerkSignupActivity.this, LoginActivity.class));
                            finish();
                        } else {
                            progressdialog.dismiss();
                            String message = object.getString("message");
                            Toast.makeText(ClerkSignupActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressdialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressdialog.dismiss();
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (volleyError instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (ClerkSignupActivity.this != null)
                        Toast.makeText(ClerkSignupActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", full_name);
                    params.put("email", email);
                    params.put("password", password);
                    params.put("photo","data:image/jpeg;base64,"+input);
                    params.put("phone", phone);

                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept", "application/json");
                    return params;
                }

            };

            RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                    25000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySingleton.getInstance(ClerkSignupActivity.this).addToRequestQueue(RegistrationRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(ivProfile);
        ivProfile.setColorFilter(ContextCompat.getColor(ClerkSignupActivity.this, android.R.color.transparent));
    }

    private void onProfileImageClick() {
        Dexter.withActivity(ClerkSignupActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(ClerkSignupActivity.this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(ClerkSignupActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(ClerkSignupActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                photo_chnage = true;
                uri = data.getParcelableExtra("path");

                // You can update this bitmap to your server
                // loading profile image from local cache
                loadProfile(uri.toString());
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ClerkSignupActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // SignupFragment.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

}
