package com.example.queueme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.queueme.R;

public class SplashActivityStart extends AppCompatActivity {
ImageView ivLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_start);
        ivLogo=findViewById(R.id.ivLogo);
        updateUi();
    }


    private void updateUi() {
        slideDownAnim();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                showButtons();
                startActivity(new Intent(SplashActivityStart.this,SplashActivity.class));
                finish();
            }
        }, 1200);
    }

    private void slideDownAnim() {
//        ivLogo.setVisibility(View.VISIBLE);
        Animation slideUpAnimation = AnimationUtils.loadAnimation(SplashActivityStart.this,
                R.anim.slide_bottom_animation);
        ivLogo.startAnimation(slideUpAnimation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                showButtons();
            }
        }, 850);
        ivLogo.setVisibility(View.VISIBLE);
        Animation slid = AnimationUtils.loadAnimation(SplashActivityStart.this,
                R.anim.slide_up_animation);
        ivLogo.startAnimation(slid);
    }
}
