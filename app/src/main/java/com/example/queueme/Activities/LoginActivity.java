package com.example.queueme.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText etEmail, password;
    TextView reset_pass, signup, tvLogin;
    Button login_btn;
    String u_email, u_pass;
    public static String abc;
    LinearLayout llSignUp;
    String phot,login_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        clickViews();
    }

    //initializing Views
    private void initViews() {
        login_btn = findViewById(R.id.login_btn);
        reset_pass = findViewById(R.id.resetPass);
        tvLogin = findViewById(R.id.tvLogin);
        signup = findViewById(R.id.signup);
        etEmail = findViewById(R.id.etEmail);
        llSignUp = findViewById(R.id.llSignUp);
        password = findViewById(R.id.password);
         login_status = Utilities.getString(LoginActivity.this, "login");
        if (login_status.equals("Admin")) {
//            llSignUp.setVisibility(View.GONE);
//            startActivity(new Intent(LoginActivity.this,AdminSignupActivity.class));
            tvLogin.setText("Login as Store Admin");

        } else if (login_status.equals("Clerk")) {
//            llSignUp.setVisibility(View.GONE);
            tvLogin.setText("Login as Store Clerk");

        } else if (login_status.equals("User")) {
            tvLogin.setText("Login as User");

        }

    }

    private void clickViews() {

        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassActivity.class));
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (login_status.equals("Admin")) {
//            llSignUp.setVisibility(View.GONE);
            startActivity(new Intent(LoginActivity.this,AdminSignupActivity.class));
            finish();

                } else if (login_status.equals("Clerk")) {
                    startActivity(new Intent(LoginActivity.this,ClerkSignupActivity.class));
                    finish();

                } else if (login_status.equals("User")) {
//                    tvLogin.setText("Login as User");
                    startActivity(new Intent(LoginActivity.this, Signup1Activity.class));
                    finish();
                }


            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                u_email = etEmail.getText().toString().trim();
                u_pass = password.getText().toString().trim();
                Utilities.saveString(LoginActivity.this, "email", u_email);
                Utilities.saveString(LoginActivity.this, "pass", u_pass);
                if (!u_email.isEmpty()) {
                    if (!u_pass.isEmpty()) {

                        //API Here
                        loginApiCall(u_email, u_pass);
//                        Toast.makeText(LoginActivity.this, "Login Api", Toast.LENGTH_SHORT).show();
                    } else {
                        password.setError("Field Required");
                    }
                } else {
                    etEmail.setError("Field Required");
                }

            }
        });
    }


    private void loginApiCall(final String email, final String password) {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(LoginActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        JSONObject obj = object.getJSONObject("data");
                        SessionManager sessionManager = new SessionManager(LoginActivity.this);

                        int id = obj.getInt("id");
                        String name = obj.getString("name");
                        String phone = obj.getString("phone");
                        String abc = obj.getString("user_role");
                        if (obj.has("image")) {

                            phot = obj.getString("image");

                        }


                        progressdialog.dismiss();

//                        String photoo = "";


//                        for (int i = 0; i < user_group.length(); i++) {
//
//                            abc = user_group.getString(i);
//                        }


                        String login_status = Utilities.getString(LoginActivity.this, "login");
                        if (login_status.equals(abc)) {
                            Utilities.saveString(LoginActivity.this, "user_group", abc);
                            Utilities.saveString(LoginActivity.this, "login_status", "true");
                            Utilities.saveString(LoginActivity.this, "photo", phot);
                            Utilities.saveString(LoginActivity.this, "id", String.valueOf(id));
                            Utilities.saveString(LoginActivity.this, "userName", name);
                            Utilities.saveString(LoginActivity.this, "phone", phone);
                            if (abc.equals("Store Admin")||abc.equals("Admin")) {
                                progressdialog.dismiss();
                                Intent intent = new Intent(LoginActivity.this,AdminActivity.class);
                                startActivity(intent);
                                finish();

                            } else if (abc.equals("Clerk")) {
                                progressdialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, LandingActivity.class));
                                finish();

                            } else if (abc.equals("User")) {
                                progressdialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, UserActivity.class));
                                finish();

                            }
                        } else {
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("Message")
                                    .setMessage("Please Login from "+ abc +"'s login page" )
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }

                        sessionManager.createLoginSession(name, email, String.valueOf(id), abc, phot);

                    } else {
                        String message = object.getString("message");
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (LoginActivity.this != null)
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
//

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }


        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(LoginActivity.this).addToRequestQueue(RegistrationRequest);


    }


}
