package com.example.queueme.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogoutToLoginActivity extends AppCompatActivity {
    EditText etEmail, password;
    TextView reset_pass, signup, tvLogin;
    Button login_btn;
    String u_email, u_pass;
    public static String abc;
    LinearLayout llSignUp;
    String phot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        clickViews();
    }

    //initializing Views
    private void initViews() {
        login_btn = findViewById(R.id.login_btn);
        reset_pass = findViewById(R.id.resetPass);
        tvLogin = findViewById(R.id.tvLogin);
        signup = findViewById(R.id.signup);
        etEmail = findViewById(R.id.etEmail);
        llSignUp = findViewById(R.id.llSignUp);
        password = findViewById(R.id.password);

    }

    private void clickViews() {

        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogoutToLoginActivity.this, ForgotPassActivity.class));
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogoutToLoginActivity.this, Signup1Activity.class));
                finish();
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                u_email = etEmail.getText().toString().trim();
                u_pass = password.getText().toString().trim();
                Utilities.saveString(LogoutToLoginActivity.this, "email", u_email);
                Utilities.saveString(LogoutToLoginActivity.this, "pass", u_pass);
                if (!u_email.isEmpty()) {
                    if (!u_pass.isEmpty()) {

                        //API Here
                        loginApiCall(u_email, u_pass);
//                        Toast.makeText(LoginActivity.this, "Login Api", Toast.LENGTH_SHORT).show();
                    } else {
                        password.setError("Field Required");
                    }
                } else {
                    etEmail.setError("Field Required");
                }

            }
        });
    }


    private void loginApiCall(final String email, final String password) {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(LogoutToLoginActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        JSONObject obj = object.getJSONObject("result");
                        SessionManager sessionManager = new SessionManager(LogoutToLoginActivity.this);

                        String id = obj.getString("id");
                        String name = obj.getString("name");
                        String phone = obj.getString("phone");
                        if (obj.has("photo")) {

                            phot = obj.getString("photo");

                        }


                        progressdialog.dismiss();

//                        String photoo = "";
                        JSONArray user_group = obj.getJSONArray("user_group");

                        for (int i = 0; i < user_group.length(); i++) {

                            abc = user_group.getString(i);
                        }
                            Utilities.saveString(LogoutToLoginActivity.this, "user_group", abc);
                            Utilities.saveString(LogoutToLoginActivity.this, "login_status", "true");
                            Utilities.saveString(LogoutToLoginActivity.this, "photo", phot);
                            Utilities.saveString(LogoutToLoginActivity.this, "id", id);
                            Utilities.saveString(LogoutToLoginActivity.this, "userName", name);
                            Utilities.saveString(LogoutToLoginActivity.this, "phone", phone);
                            if (abc.equals("Store Admin")) {
                                progressdialog.dismiss();
                                Intent intent = new Intent(LogoutToLoginActivity.this,AdminActivity.class);
                                startActivity(intent);
                                finish();

                            } else if (abc.equals("Clerk")) {
                                progressdialog.dismiss();
                                startActivity(new Intent(LogoutToLoginActivity.this, LandingActivity.class));
                                finish();

                            } else if (abc.equals("User")) {
                                progressdialog.dismiss();
                                startActivity(new Intent(LogoutToLoginActivity.this, UserActivity.class));
                                finish();

                            }


                        sessionManager.createLoginSession(name, email, id, abc, phot);

                    } else {
                        String message = object.getString("message");
                        Toast.makeText(LogoutToLoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (LogoutToLoginActivity.this != null)
                    Toast.makeText(LogoutToLoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "login");
                params.put("apikey", "mtechapi12345");
                params.put("email", email);
                params.put("password", password);
//

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(LogoutToLoginActivity.this).addToRequestQueue(RegistrationRequest);


    }


}
