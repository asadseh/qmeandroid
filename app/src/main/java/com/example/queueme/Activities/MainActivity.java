package com.example.queueme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Fragments.SettingFragment;
import com.example.queueme.Fragments.UserStoreFragment;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FragmentActivity {
    ImageView ivClose;
    Button bSubmit;
    EditText etEnterCode;
    String shop_code;
    //    String shop_name;
    FrameLayout fl;
    SurfaceView cameraView;
    BarcodeDetector barcode;
    LinearLayout llsurfaceView;
    CameraSource cameraSource;
    SurfaceHolder holder;
    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;
    public static ArrayList<UserStoreModel> userStoreModels;

    //View Objects
    private TextView buttonScan;
    private TextView textViewName, textViewAddress;

    //qr code scanner object
    private IntentIntegrator qrScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //View objects
        buttonScan = findViewById(R.id.tvQrScan);
        textViewName = (TextView) findViewById(R.id.tvName);
//        textViewAddress = (TextView) findViewById(R.id.Code);
        bSubmit = findViewById(R.id.bSubmit);
        ivClose = findViewById(R.id.cancel_action);
        etEnterCode = findViewById(R.id.etEnterCode);
        cameraView = findViewById(R.id.cameraView1);
        llsurfaceView = findViewById(R.id.llsurfaceView);

        userStoreModels = new ArrayList<>();

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shopCodee = etEnterCode.getText().toString();
                Utilities.saveString(MainActivity.this, "shop_code1", shopCodee);
//                String shopName = userStoreModels.get(0).getShopName();
//                Utilities.saveString(MainActivity.this, "shopNameee", shopName);
                getUserQQApi(shopCodee);
//                loadFragment();


            }
        });
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
//        }
        cameraView.setZOrderMediaOverlay(true);
        holder = cameraView.getHolder();
        barcode = new BarcodeDetector.Builder(MainActivity.this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        if (!barcode.isOperational()) {
            Toast.makeText(getApplicationContext(), "Sorry, Couldn't setup the detector", Toast.LENGTH_LONG).show();
            MainActivity.this.finish();
        }

        cameraSource = new CameraSource.Builder(MainActivity.this, barcode)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(30)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1280, 960)
                .build();

        barcode.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {
                    etEnterCode.post(new Runnable() {
                        @Override
                        public void run() {

                            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(1000);
                            etEnterCode.setText(barcodes.valueAt(0).displayValue);
//                            llsurfaceView.setVisibility(View.VISIBLE);
//                            cameraView.setVisibility(View.GONE);
                        }
                    });
                }
            }
        });


        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(cameraView.getHolder());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });


        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llsurfaceView.setVisibility(View.GONE);
                cameraView.setVisibility(View.VISIBLE);
            }
        });

    }

    //Getting the scan results
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (result != null) {
//            //if qrcode has nothing in it
//            if (result.getContents() == null) {
//                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
//            } else {
//                //if qr contains data
//                try {
//                    //converting the data to json
//                    JSONObject obj = new JSONObject(result.getContents());
//                    //setting values to textviews
//                    etEnterCode.setText(obj.getString("name"));
//                    etEnterCode.setText(obj.getString("address"));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    //if control comes here
//                    //that means the encoded format not matches
//                    //in this case you can display whatever data is available on the qrcode
//                    //to a toast
//                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
//                    etEnterCode.setText(result.getContents());
//                }
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

//    @Override
//    public void onClick(View view) {
//        //initiating the qr code scan
//        qrScan.initiateScan();
//    }

    public void loadFragment() {
        fl.setVisibility(View.VISIBLE);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.user_container, new UserStoreFragment());
        ft.commit();
    }

    private void getUserQQApi(final String shop_coddd) {

//        shop_code = Utilities.getString(getContext(), "editShopCode");

        final ProgressDialog progressdialog = new ProgressDialog(MainActivity.this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        final JSONArray objUser = object.getJSONArray("result");
                        if (objUser.length() > 0) {
                            for (int i = 0; i < objUser.length(); i++) {
//
                                JSONObject jsonObject = objUser.getJSONObject(i);

                                String name = jsonObject.getString("queue_name");
                                String shop_name = jsonObject.getString("shop_name");
                                String shp_id = jsonObject.getString("shop");
                                String noOfQ = jsonObject.getString("number_of_persons");
                                userStoreModels.add(new UserStoreModel(name, noOfQ, shop_name, shp_id));
                                Utilities.saveString(MainActivity.this, "shopname", shop_name);
                                Utilities.saveString(MainActivity.this, "shopIdd", shp_id);

                                progressdialog.dismiss();

//
                            }
                            Intent intent = new Intent(MainActivity.this, DemUserStoreActivity.class);
//                            intent.putExtra("shop_name",shop_name);
//                            String a = shop_name;
                            startActivity(intent);
                            finish();
                        } else {
                            Utilities.hideProgressDialog();
                            Toast.makeText(MainActivity.this, "null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Utilities.hideProgressDialog();
                        Toast.makeText(MainActivity.this, "null Data", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (MainActivity.this != null)
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "queue_list_by_shopcode");
                params.put("apikey", "mtechapi12345");
                params.put("shop_code", shop_coddd);
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(MainActivity.this).addToRequestQueue(qmeRequest);
    }

}