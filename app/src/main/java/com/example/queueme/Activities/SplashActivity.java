package com.example.queueme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.queueme.Models.UserGroupModel;
import com.example.queueme.Models.UserStoreModel;
import com.example.queueme.R;
import com.example.queueme.SessionManager.SessionManager;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddShopDialog;

import java.util.ArrayList;


public class SplashActivity extends AppCompatActivity {
    ImageView ivLogo;
    TextView appName;
    private static Button bClerk, bAdmin, bUser;
    LinearLayout llButton;
    String userGroupModels, user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ivLogo = findViewById(R.id.ivLogo);
        llButton = findViewById(R.id.llButton);
        bClerk = findViewById(R.id.clerk);
        bAdmin = findViewById(R.id.admin);
        bUser = findViewById(R.id.user);
        user = Utilities.getString(SplashActivity.this, "user_type");
//        slideDowsnAnim();


//        ClickViews();

//        String login_status = Utilities.getString(SplashActivity.this,"login_status");
//        if (login_status.equals("true")){
//
//            String user_type =  Utilities.getString(SplashActivity.this,"user_group");
//
//            if (user_type.equals("User")){
//                startActivity(new Intent(SplashActivity.this, UserActivity.class));
//                finish();
//            }
//            if (user_type.equals("Store Admin")){
//                startActivity(new Intent(SplashActivity.this, AdminActivity.class));
//                finish();
//            }if (user_type.equals("Clerk")){
//                startActivity(new Intent(SplashActivity.this, LandingActivity.class));
//                finish();
//            }
//        }else { }


//        SessionManager sessionManager = new SessionManager(getApplicationContext());
//        if (sessionManager.isLoggedIn()) {
//            userGroupModels = SessionManager.USER_GROUP;
//
//            if (userGroupModels.equals("User")){
//            startActivity(new Intent(SplashActivity.this, UserActivity.class));
//            finish();
//        }    else if (userGroupModels.equals("Store Admin")){
//            startActivity(new Intent(SplashActivity.this, AdminActivity.class));
//            finish();
//        }    else if (userGroupModels.equals("Clerk")){
//            startActivity(new Intent(SplashActivity.this, LandingActivity.class));
//            finish();
//        }
//
//
//        }
//
//        updateUiii();
//        ToNextActivity();

    }

    private void ClickViews() {

        bAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utilities.saveString(SplashActivity.this, "user_type", "Admin");
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        });
        bClerk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.saveString(SplashActivity.this, "user_type", "Clerk");
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        });
        bUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.saveString(SplashActivity.this, "user_type", "User");
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    private void updateUiii() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showButtons();
            }
        }, 150);


    }

    private void ToNextActivity() {
        if (!user.isEmpty()) {
            switch (user) {
                case "Admin":
//                    slideDowsnAnim();
                    startActivity(new Intent(SplashActivity.this, AdminActivity.class));
                    finish();
                    break;
                case "Clerk":
//                    slideDowsnAnim();
                    startActivity(new Intent(SplashActivity.this, LandingActivity.class));
                    finish();
                    break;
                case "User":
//                    slideDowsnAnim();
                    startActivity(new Intent(SplashActivity.this, UserActivity.class));
                    finish();
                    break;
            }
        } else {
            updateUiii();
        }
    }




    private void showButtons() {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(SplashActivity.this,
                R.anim.slide_left_to_right_animation);
        llButton.setVisibility(View.VISIBLE);

        llButton.startAnimation(slideUpAnimation);
    }
//    private void updateUi() {
////        slideDownAnim();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                showButtons();
//            }
//        }, 850);
//    }
//    private void slideDownAnim() {
////        ivLogo.setVisibility(View.VISIBLE);
////        Animation slideUpAnimation = AnimationUtils.loadAnimation(SplashActivity.this,
////                R.anim.slide_bottom_animation);
////        ivLogo.startAnimation(slideUpAnimation);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                showButtons();
//            }
//        }, 850);
////        ivLogo.setVisibility(View.VISIBLE);
////        Animation slid = AnimationUtils.loadAnimation(SplashActivity.this,
////                R.anim.slide_up_animation);
////        ivLogo.startAnimation(slid);
//    }

//    private void slideupAnim() {
//        ivLogo.setVisibility(View.VISIBLE);
//        Animation slideUpAnimations = AnimationUtils.loadAnimation(SplashActivity.this,
//                R.anim.slide_up_animation);
//        appName.startAnimation(slideUpAnimations);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//            }
//        }, 900);
//        appName.setVisibility(View.VISIBLE);
//        Animation slide = AnimationUtils.loadAnimation(SplashActivity.this,
//                R.anim.slide_bottom_animation);
//        appName.startAnimation(slide);
//    }
//private void updateUi() {
//    slideDownAnim();
//    new Handler().postDelayed(new Runnable() {
//        @Override
//        public void run() {
////                showButtons();
//            finish();
//        }
//    }, 1200);
//}

//    private void slideDowsnAnim() {
//        ivLogo.setVisibility(View.VISIBLE);
//        Animation slideUpAnimation = AnimationUtils.loadAnimation(SplashActivity.this,
//                R.anim.slide_bottom_animation);
//        ivLogo.startAnimation(slideUpAnimation);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                showButtons();
//            }
//        }, 850);
//        ivLogo.setVisibility(View.VISIBLE);
//        Animation slid = AnimationUtils.loadAnimation(SplashActivity.this,
//                R.anim.slide_up_animation);
//        ivLogo.startAnimation(slid);
//    }
}
