package com.example.queueme.Models;

public class UserStoreModel {

    private String QName;
    String TotalQ;
    String ShopName;
    String shopId;

    public UserStoreModel(String QName, String totalQ, String shopName, String shopId) {
        this.QName = QName;
        TotalQ = totalQ;
        ShopName = shopName;
        this.shopId = shopId;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getQName() {
        return QName;
    }

    public void setQName(String QName) {
        this.QName = QName;
    }


    public String getTotalQ() {
        return TotalQ;
    }

    public void setTotalQ(String totalQ) {
        TotalQ = totalQ;
    }
}
