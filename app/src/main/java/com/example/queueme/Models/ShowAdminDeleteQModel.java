package com.example.queueme.Models;

public class ShowAdminDeleteQModel {

    private String Qname;
    String Qid;
    String opening_time;
    String closing_time;

    public ShowAdminDeleteQModel(String qname, String qid, String opening_time, String closing_time) {
        Qname = qname;
        Qid = qid;
        this.opening_time = opening_time;
        this.closing_time = closing_time;
    }

    public String getQname() {
        return Qname;
    }

    public void setQname(String qname) {
        Qname = qname;
    }

    public String getQid() {
        return Qid;
    }

    public void setQid(String qid) {
        Qid = qid;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }
}
