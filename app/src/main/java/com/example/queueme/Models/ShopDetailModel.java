package com.example.queueme.Models;

public class ShopDetailModel {

    private String name;
    private String NoOfPersons;
    String Qid;
    String opening_time;
    String closing_time;
    String status;
    String shopId;

    public ShopDetailModel(String name, String noOfPersons, String qid, String opening_time, String closing_time, String status, String shopId) {
        this.name = name;
        NoOfPersons = noOfPersons;
        Qid = qid;
        this.opening_time = opening_time;
        this.closing_time = closing_time;
        this.status = status;
        this.shopId = shopId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }



    public String getNoOfPersons() {
        return NoOfPersons;
    }


    public String getQid() {
        return Qid;
    }

    public void setQid(String qid) {
        Qid = qid;
    }

    public void setNoOfPersons(String noOfPersons) {
        NoOfPersons = noOfPersons;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
