package com.example.queueme.Models;

public class UserHomeModel {
    String userShopId;
    private String userShopName;
    String totalNoOfQ;
    String shop_code;
    String followId;
    String closeTime,OpenTime,closeDay,openDay;
String image;

    public UserHomeModel(String userShopId, String userShopName, String totalNoOfQ, String shop_code, String followId, String closeTime, String openTime, String closeDay, String openDay, String image) {
        this.userShopId = userShopId;
        this.userShopName = userShopName;
        this.totalNoOfQ = totalNoOfQ;
        this.shop_code = shop_code;
        this.followId = followId;
        this.closeTime = closeTime;
        OpenTime = openTime;
        this.closeDay = closeDay;
        this.openDay = openDay;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getShop_code() {
        return shop_code;
    }

    public String getFollowId() {
        return followId;
    }

    public void setFollowId(String followId) {
        this.followId = followId;
    }

    public void setShop_code(String shop_code) {
        this.shop_code = shop_code;
    }

    public String getUserShopId() {
        return userShopId;
    }

    public void setUserShopId(String userShopId) {
        this.userShopId = userShopId;
    }

    public String getUserShopName() {
        return userShopName;
    }

    public void setUserShopName(String userShopName) {
        this.userShopName = userShopName;
    }

    public String getTotalNoOfQ() {
        return totalNoOfQ;
    }

    public void setTotalNoOfQ(String totalNoOfQ) {
        this.totalNoOfQ = totalNoOfQ;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getOpenTime() {
        return OpenTime;
    }

    public void setOpenTime(String openTime) {
        OpenTime = openTime;
    }

    public String getCloseDay() {
        return closeDay;
    }

    public void setCloseDay(String closeDay) {
        this.closeDay = closeDay;
    }

    public String getOpenDay() {
        return openDay;
    }

    public void setOpenDay(String openDay) {
        this.openDay = openDay;
    }
}
