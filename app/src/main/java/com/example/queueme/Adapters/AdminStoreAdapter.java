package com.example.queueme.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.queueme.Activities.LandingActivity;
import com.example.queueme.Activities.NotificationActivity;
import com.example.queueme.Models.AdminStoreModel;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.Models.ShopDetailModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import libs.mjn.prettydialog.PrettyDialog;

public class AdminStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private Callback callback;
    String p_no = "";
    String q_idd = "";
    String adminid, shopName, qName, clerkid, clerkName, statusqid, openTime, closeTime, shopid;
    private ArrayList<AdminStoreModel> adminStoreModels;

    public AdminStoreAdapter(Context context, ArrayList<AdminStoreModel> adminStoreModels) {
        this.context = context;
        this.adminStoreModels = adminStoreModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_shop_detail, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.tvShopName.setText(adminStoreModels.get(position).getName());
        holder1.tvNoOfQ.setText(adminStoreModels.get(position).getNoOfPersons());
        holder1.tvStartTime.setText(adminStoreModels.get(position).getOpening_time());
        holder1.tvEndTime.setText(adminStoreModels.get(position).getClosing_time());

        holder1.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                q_idd = adminStoreModels.get(position).getQid();
                shopid = adminStoreModels.get(position).getShopId();
                shopid = Utilities.getString(context, "AshopId");

                p_no = holder1.tvNoOfQ.getText().toString();
                int p_noo = Integer.parseInt(p_no) + 1;

                holder1.tvNoOfQ.setText(String.valueOf(p_noo));
                updateIncrementpersonQApiApi(q_idd, String.valueOf(p_noo));
            }
        });
        holder1.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q_idd = adminStoreModels.get(position).getQid();
                p_no = holder1.tvNoOfQ.getText().toString();
                shopid = adminStoreModels.get(position).getShopId();
                shopName = adminStoreModels.get(position).getShopname();
                int p_noo = Integer.parseInt(p_no) - 1;

                holder1.tvNoOfQ.setText(String.valueOf(p_noo));
                updateDecrementpersonQApiApi(q_idd, String.valueOf(p_noo));

            }
        });
        if (adminStoreModels.get(position).getStatus().equals("Active")) {
            holder1.qSwitch.setChecked(true);
        } else {
            holder1.qSwitch.setChecked(false);

        }


        holder1.qSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {

                q_idd = adminStoreModels.get(position).getQid();
                if (holder1.qSwitch.isChecked()) {
                    q_idd = adminStoreModels.get(position).getQid();
                    qName = adminStoreModels.get(0).getName();
                    shopid = adminStoreModels.get(0).getShopId();
                    UpdateQ();
                } else {
                    q_idd = adminStoreModels.get(position).getQid();
                    qName = adminStoreModels.get(position).getName();
                    shopid = adminStoreModels.get(position).getShopId();
                    UpdateStatusOFQ();
                }

            }

        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return adminStoreModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlus, tvMinus, tvNoOfQ, tvShopName, tvStartTime, tvEndTime;
        LinearLayout llItem;
        Switch qSwitch;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMinus = itemView.findViewById(R.id.minus);
            tvPlus = itemView.findViewById(R.id.plus);
            tvShopName = itemView.findViewById(R.id.tvName);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            tvEndTime = itemView.findViewById(R.id.tvEndTime);
            tvNoOfQ = itemView.findViewById(R.id.tvNoOfQ);
            qSwitch = itemView.findViewById(R.id.qSwitch);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            AdminStoreModel poem = adminStoreModels.get(pos);
//            tvShopName.setText(poem.getShopName());
//            tvNoOfQ.setText(poem.getNoOfQ());
//            initClickListener();
        }

        private void initClickListener() {
//
//            llItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//                        callback.onItemClick(getAdapterPosition());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }

    private void updateIncrementpersonQApiApi(final String qu_id, final String p_number) {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/update_queue_person", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();


                        progressdialog.dismiss();

                        Toast.makeText(context, "success", Toast.LENGTH_LONG);


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("queue_id", qu_id);
                params.put("number_of_persons", p_number);
                params.put("shop_id", shopid);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }

    private void UpdateQ() {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/update_queue_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();

                        Toast.makeText(context, "Status Active", Toast.LENGTH_SHORT).show();
                        progressdialog.dismiss();


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("status", "Active");
                params.put("queue_id", q_idd);
                params.put("queue_name", qName);
                params.put("shop_id", shopid);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }

    private void UpdateStatusOFQ() {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/update_queue_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();


                        progressdialog.dismiss();
                        Toast.makeText(context, "Status InActive", Toast.LENGTH_SHORT).show();


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("status", "Inactive");
                params.put("queue_id", q_idd);
                params.put("queue_name", qName);
                params.put("shop_id", shopid);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }

    private void updateDecrementpersonQApiApi(final String qu_id, final String p_number) {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/update_queue_person", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();


                        progressdialog.dismiss();

                        Toast.makeText(context, "success", Toast.LENGTH_LONG);


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("queue_id", qu_id);
                params.put("number_of_persons", p_number);
                params.put("shop_id", shopid);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }
}
