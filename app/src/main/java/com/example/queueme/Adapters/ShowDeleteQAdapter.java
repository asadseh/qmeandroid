package com.example.queueme.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Models.ShopDetailModel;
import com.example.queueme.Models.ShowDeleteQModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowDeleteQAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private Callback callback;
    String q_idd = "";
    private ArrayList<ShowDeleteQModel> showDeleteQModels;
String user_id,clerkName,Qid;
    public ShowDeleteQAdapter(Context context, ArrayList<ShowDeleteQModel> showDeleteQModels) {
        this.context = context;
        this.showDeleteQModels = showDeleteQModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_delete_q, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvName.setText(showDeleteQModels.get(position).getQname());
        holder1.tvStartTime.setText(showDeleteQModels.get(position).getOpening_time());
        holder1.tvEndTime.setText(showDeleteQModels.get(position).getClosing_time());
        holder1.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                q_idd = showDeleteQModels.get(position).getQid();
                DeletQApiApi();
            }
        });
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return showDeleteQModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlus, tvMinus, tvNoOfQ, tvName,tvStartTime,tvEndTime;
        LinearLayout llItem,llDelete;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvQName);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            tvEndTime = itemView.findViewById(R.id.tvEndTime);
            llItem = itemView.findViewById(R.id.llItem);
            llDelete = itemView.findViewById(R.id.llDelete);

        }

        private void bind(int pos) {
            ShowDeleteQModel poem = showDeleteQModels.get(pos);
//            tvName.setText(poem.getName());
//            tvNoOfQ.setText(poem.getNoOfPersons());
//            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        callback.onItemClick(getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }

    private void AddQApiApi() {

        user_id= Utilities.getString(context,"id");
        clerkName=Utilities.getString(context,"name");
//        shop_code=Utilities.getString(getActivity(), "shopCode");
//        shopid=Utilities.getString(getActivity(), "shopId");

        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        String message = object.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                        progressdialog.dismiss();

                        Toast.makeText(context, "success", Toast.LENGTH_LONG).show();


                    } else {
                        String message = object.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "add_queue");
                params.put("apikey", "mtechapi12345");
                params.put("store_admin_id", Utilities.getString(context,"store_admin_id"));
                params.put("shop", Utilities.getString(context,"shopsId"));
                params.put("shop_name", Utilities.getString(context,"shopName"));
//                params.put("name", "new queue");
//                params.put("number_of_persons", "3");
                params.put("clerk", user_id);
                params.put("clerk_name", clerkName);
                params.put("status", "Active");
//                params.put("opening_time", "02:29");
//                params.put("closing_time", "02:20");
                params.put("shop_code", Utilities.getString(context,"shop_code"));

                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }

    private void DeletQApiApi() {

        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/delete_queue", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                        progressdialog.dismiss();

                        Toast.makeText(context, message, Toast.LENGTH_LONG);


                    } else {
                        String message = object.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("queue_id", q_idd);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }
}
