package com.example.queueme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.queueme.Activities.AdminActivity;
import com.example.queueme.Activities.LandingActivity;
import com.example.queueme.Models.ClerkModel;
import com.example.queueme.R;
import com.example.queueme.Utility.Utilities;
import com.example.queueme.dialogs.AddQDialog;
import com.example.queueme.dialogs.DeleteQDialog;

import java.util.ArrayList;

public class ClerkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private Callback callback;
    private ArrayList<ClerkModel> clerkModels;
    String shopid,q_idd;

    public ClerkAdapter(Context context, ArrayList<ClerkModel> clerkModels,Callback callback) {
        this.context = context;
        this.callback = callback;
        this.clerkModels = clerkModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvShopName.setText(clerkModels.get(position).getShopName());
        holder1.noOfQ.setText(clerkModels.get(position).getNoOfQ());


        holder1.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shopp_idd = clerkModels.get(position).getShopId();
                String store_admin_id = clerkModels.get(position).getStore_admin_id();
                String shop_code = clerkModels.get(position).getShop_code();

                Utilities.saveString(context,"shopp_id_clerkwali",shopp_idd);
                Utilities.saveString(context,"store_admin_id",store_admin_id);
                Utilities.saveString(context,"shop_code",shop_code);

                showAddQDialog();
            }
        });
        holder1.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String shopp_idd = clerkModels.get(position).getShopId();

                Utilities.saveString(context,"shopp_id_clerkwali",shopp_idd);

                showDeleteQDialog();
            }
        });
//        holder1.llItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((LandingActivity)context).navController.navigate(R.id.action_homeFragment_to_shop_detail_Fragment);
//
//            }
//        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return clerkModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlus;
        TextView tvMinus;
        TextView tvShopName;
        TextView noOfQ;
        LinearLayout llItem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMinus = itemView.findViewById(R.id.minus);
            tvPlus = itemView.findViewById(R.id.plus);
            tvShopName = itemView.findViewById(R.id.tvShopName);
            noOfQ = itemView.findViewById(R.id.noOfQ);
            llItem = itemView.findViewById(R.id.llItem);

        }

        private void bind(int pos) {
            ClerkModel poem = clerkModels.get(pos);
            tvShopName.setText(poem.getShopName());
            initClickListener();
        }

        private void initClickListener() {
//
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }
    private void showAddQDialog() {
        FragmentActivity activity = (FragmentActivity) (context);
        FragmentManager fm = activity.getSupportFragmentManager();
        AddQDialog alertDialog = new AddQDialog();
        alertDialog.show(fm, "fragment_alert");

    }
    private void showDeleteQDialog() {
        FragmentActivity activity = (FragmentActivity) (context);
        FragmentManager fm = activity.getSupportFragmentManager();
        DeleteQDialog alertDialog = new DeleteQDialog();
        alertDialog.show(fm, "fragment_alert");

    }
}
