package com.example.queueme.adminAdapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.queueme.Models.FollowReqModel;
import com.example.queueme.Models.ShopDetailModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class FollowReqAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private Callback callback;
    String type;
    String followid="";

    private ArrayList<FollowReqModel> followReqModels;

    public FollowReqAdapter(Context context, ArrayList<FollowReqModel> followReqModels) {
        this.context = context;
        this.followReqModels = followReqModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_follow_request, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;
        String followStatus=followReqModels.get(position).getFollow_status();
        if(followStatus.equals("Accepted")||followStatus.equals("Rejected")){
            holder1.llAcceotDecline1.setVisibility(View.VISIBLE);
            holder1.llAcceotDecline.setVisibility(View.GONE);

        }else if(followStatus.equals("Pending")){
            holder1.llAcceotDecline1.setVisibility(View.GONE);
            holder1.llAcceotDecline.setVisibility(View.VISIBLE);

        }
            holder1.tvName.setText(followReqModels.get(position).getName());
            holder1.tvStatus.setText(followReqModels.get(position).getFollow_status());
//        Picasso.get().load(.get(position).getPhoto()).into(holder1.ivThumbnail);
            Picasso.with(context).load(Server.BASE_URL_PHOTO+followReqModels.get(position).getPhoto()).into(holder1.ivprofile);
//
//        }
//        else{
//
//
//        }
        holder1.tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                followid = followReqModels.get(position).getFollowId();

                getAcceptApi();

            }
        });
        holder1.tvDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                followid = followReqModels.get(position).getFollowId();
                getRejectApi();
            }
        });


        holder1.bind(position);
    }

    @Override
    public int getItemCount() {

        return followReqModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvAccept, tvDeny,tvStatus;
        ImageView ivprofile;
        LinearLayout llItem;
        RelativeLayout llAcceotDecline1,llAcceotDecline;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAccept = itemView.findViewById(R.id.tvAccept);
            tvName = itemView.findViewById(R.id.tvName);
            tvDeny = itemView.findViewById(R.id.tvDeny);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            ivprofile = itemView.findViewById(R.id.ivProfile);
            llItem = itemView.findViewById(R.id.llItem);
            llAcceotDecline = itemView.findViewById(R.id.llAcceotDecline);
            llAcceotDecline1 = itemView.findViewById(R.id.llAcceotDecline1);
            tvName.setSelected(true);

        }

        private void bind(int pos) {
            FollowReqModel poem = followReqModels.get(pos);
//            tvName.setText(poem.getName());
//            tvNoOfQ.setText(poem.getNoOfQ());
//            initClickListener();
        }

        private void initClickListener() {
//
//            llItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try {
//                        callback.onItemClick(getAdapterPosition());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

    public interface Callback {
        void onItemClick(int pos) throws JSONException;
    }

    private void getAcceptApi() {

        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        String message = object.getString("message");
                        progressdialog.dismiss();
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();




                        Toast.makeText(context, "success", Toast.LENGTH_LONG);


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "accept_request");
                params.put("apikey", "mtechapi12345");
                params.put("id", followid);

                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }

    private void getRejectApi() {
//        user_id= Utilities.getString(getActivity(), "id");
//        shop_code=Utilities.getString(getActivity(), "shopCode");
//        shopid=Utilities.getString(getActivity(), "shopId");

        final ProgressDialog progressdialog = new ProgressDialog(context);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
                        String message = object.getString("message");



                        progressdialog.dismiss();
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(context)
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (context != null)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "reject_request");
                params.put("apikey", "mtechapi12345");
                params.put("id", followid);

                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
//                return params;
//            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(context).addToRequestQueue(qmeRequest);
    }
}
