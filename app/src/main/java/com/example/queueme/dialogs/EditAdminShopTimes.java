package com.example.queueme.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;
import com.lany.picker.HourMinutePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;


public class EditAdminShopTimes extends DialogFragment {
    ImageView ivClose;
    TimePicker timePicker1, timePicker2;
    Calendar calendar;
    HourMinutePicker hourMinutePicker, hourMinutePicker2;
    String format;
    TextView time, tvEndTime;
    Button bSaveTime;
    String startTime;
    Button btnGet;
    String shopid, timeSet, openingTime, h, m, image, shopName, openDay, closeDay, store_adminId, startDay, endDay, shop_code;

    StringBuilder endTime, startsTime;

    public static EditAdminShopTimes newInstance(int title) {
        EditAdminShopTimes frag = new EditAdminShopTimes();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.edit_times_dialog_box, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hourMinutePicker = view.findViewById(R.id.hour_minute_picker1);
        hourMinutePicker2 = view.findViewById(R.id.hour_minute_picker2);
        bSaveTime = view.findViewById(R.id.bSaveTime);
        ivClose = view.findViewById(R.id.cancel_action);
        tvEndTime = view.findViewById(R.id.tvEndTime);
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
        shopName = Utilities.getString(getContext(), "Asshop_Name");
        shopid = Utilities.getString(getContext(), "AshopId");
        openDay = Utilities.getString(getContext(), "openDay");
        shop_code = Utilities.getString(getContext(), "shop_code");
        closeDay = Utilities.getString(getContext(), "closeDay");
        store_adminId = Utilities.getString(getContext(), "id");
        image = Utilities.getString(getContext(), "image");
//        files = Utilities.getString(getContext(), "files");
        hourMinutePicker.setIs24HourView(false);
        hourMinutePicker.setSelectionDivider(new ColorDrawable(0xff436EEE));
        hourMinutePicker.setSelectionDividerHeight(4);
        hourMinutePicker.setOnTimeChangedListener(new HourMinutePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(HourMinutePicker view, int hourOfDay, int minute) {
//                timeSet = "";
//                if (hourOfDay > 12) {
//                    hourOfDay -= 12;
//                    timeSet = "PM";
//                } else if (hourOfDay == 0) {
//                    hourOfDay += 12;
//                    timeSet = "AM";
//                } else if (hourOfDay == 12) {
//                    timeSet = "PM";
//                } else {
//                    timeSet = "AM";
//                }
                if (hourOfDay < 10) {
                    h = "0" + String.valueOf(hourOfDay);
                } else {

                    h = String.valueOf(hourOfDay);
                }
                if (hourOfDay > 12) {


                }
                if (minute < 10) {
                    m = "0" + String.valueOf(minute);
                } else {
                    m = String.valueOf(minute);
                }


                startsTime = new StringBuilder()
                        .append(h).append(":")
                        .append(m).append("")
//                        .append(timeSet)
                ;

            }
        });
        hourMinutePicker2.setIs24HourView(false);
        hourMinutePicker2.setSelectionDivider(new ColorDrawable());
        hourMinutePicker2.setSelectionDividerHeight(4);
        hourMinutePicker2.setOnTimeChangedListener(new HourMinutePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(HourMinutePicker view, int hourOfDay, int minute) {
                timeSet = "";
//                if (hourOfDay == 12) {
//                    hourOfDay += 12;
//                }
                if (hourOfDay < 10) {
                    h = "0" + String.valueOf(hourOfDay);
                } else {

                    h = String.valueOf(hourOfDay);
                }

                if (minute < 10) {
                    m = "0" + String.valueOf(minute);
                } else {
                    m = String.valueOf(minute);
                }
                endTime = new StringBuilder()
                        .append(h).append(":")
                        .append(m).append("")
//                        .append(timeSet)
                ;

            }
        });

        bSaveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                shopName = Utilities.getString(getContext(), "Asshop_Name");
//
//                time.setText(startsTime);
//                tvEndTime.setText(endTime);
//                getDialog().cancel();
                EditTimesApi();

            }
        });


    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.MyAnimation_Window;
    }

    private void EditTimesApi() {

//        Qid= Utilities.getString(context, "Qid");
        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, "http://mtecsoft.com/qme/public/api/shop_update", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        String message = object.getString("message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        new PrettyDialog(getActivity())
//                                .setTitle(message)
//                                .setIcon(R.drawable.pdlg_icon_info)
//                                .setIconTint(R.color.colorPrimary)
////                .setMessage("PrettyDialog Message")
//                                .show();
                        getDialog().cancel();
                        progressdialog.dismiss();


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_code", shop_code);
                params.put("name", shopName);
                params.put("store_admin", store_adminId);
                params.put("image", image);
                params.put("opening_day", openDay);
                params.put("closing_day", closeDay);
                params.put("opening_time", String.valueOf(startsTime));
                params.put("closing_time", String.valueOf(endTime));
                params.put("status", "Active");
                params.put("shop_id", shopid);
//                params.put("filesToUpload", files);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
}