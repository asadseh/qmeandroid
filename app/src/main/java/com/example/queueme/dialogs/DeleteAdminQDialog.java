package com.example.queueme.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.ShowAdminDeleteQAdapter;
import com.example.queueme.Adapters.ShowDeleteQAdapter;
import com.example.queueme.Models.ShowAdminDeleteQModel;
import com.example.queueme.Models.ShowDeleteQModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;


public class DeleteAdminQDialog extends DialogFragment {
    ImageView ivClose;
    private RecyclerView rvDeleteQ;
    private LinearLayoutManager mLayoutManager;
    private ShowAdminDeleteQAdapter pAdapter;
    private ArrayList<ShowAdminDeleteQModel> showAdminDeleteQModels;
    String shopid, clerkId, openingDay, openingTime, closingDay, closingTime, shopName;
    String Qid;

    public static DeleteAdminQDialog newInstance(int title) {
        DeleteAdminQDialog frag = new DeleteAdminQDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.delete_qu_dialog_box, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.pdlg_default_animation;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivClose = view.findViewById(R.id.cancel_action);
        rvDeleteQ = view.findViewById(R.id.rvDeleteQ);
        getShowAdminDeleteQApi();
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

    }

    private void getShowAdminDeleteQApi() {

        shopid = Utilities.getString(getContext(), "admin_shopp_id");
//        clerkId = Utilities.getString(getContext(), "id");

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/queue_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    showAdminDeleteQModels = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            String Qid = jsonObject.getString("id");
                            String name = jsonObject.getString("queue_name");
                            String noOfPerson = jsonObject.getString("person");

                            String closing_time = jsonObject.getString("closing_time");
                            String opening_time = jsonObject.getString("opening_time");

//                            Utilities.saveString(getContext(), "clerkQListId", Qid);
//                            Utilities.saveString(getContext(), "name", name);
//                            Utilities.saveString(getContext(), "noOfPerson", noOfPerson);

                            showAdminDeleteQModels.add(new ShowAdminDeleteQModel(name, Qid, opening_time, closing_time));

                        }
                        pAdapter = new ShowAdminDeleteQAdapter(getContext(), showAdminDeleteQModels);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvDeleteQ.setLayoutManager(linearLayoutManager);
                        rvDeleteQ.setAdapter(pAdapter);


                    } else {
                        String message = object.getString("message");
                        Utilities.hideProgressDialog();
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("shop_id", shopid);
//                params.put("clerk_id", clerkId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
//    private void DeletQApiApi() {
//
//        Qid= Utilities.getString(getActivity(), "Qid");
//
//
//        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
//        progressdialog.setIndeterminate(true);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));
//
//        progressdialog.show();
//
//
//        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                try {
//                    JSONObject object = new JSONObject(response);
//                    int status = object.getInt("success");
//                    if (status == 1) {
//                        String message = object.getString("message");
//                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//
//
//                        progressdialog.dismiss();
//
//                        Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG);
//
//
//                    } else {
//                        String message = object.getString("message");
//                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                    }
////                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressdialog.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                if (getActivity() != null)
//                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("do", "delete_queue");
//                params.put("apikey", "mtechapi12345");
//                params.put("queue_id", Qid);
//
//
//                return params;
//            }
//
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                Map<String, String> params = new HashMap<>();
////                params.put("Accept", "application/json");
////                return params;
////            }
//        };
//
//        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
//                25000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
//    }
}