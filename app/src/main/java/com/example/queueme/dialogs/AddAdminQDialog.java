package com.example.queueme.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.queueme.Adapters.SpinnerAdapterClerks;
import com.example.queueme.Adapters.UnAssignedQAdapter;
import com.example.queueme.Models.ClerkListModel;
import com.example.queueme.Models.UnAssignedModel;
import com.example.queueme.R;
import com.example.queueme.Server.MySingleton;
import com.example.queueme.Server.Server;
import com.example.queueme.Utility.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;


public class AddAdminQDialog extends DialogFragment {
    ImageView ivClose;
    TextView tvStartTime, tvEndTime, tvStartDay, tvEndDay, setValue, error;
    TimePicker picker;
    EditText etQName, etNoofQ;
    Button bAddQ;
    private List<String> clerkList;
    private ArrayList<ClerkListModel> clerkListModels;
    String user_id, clerkName, qName, noOfQ, endTime, startTime;
    Spinner spStartDay, spEndDay, spClerkList;
    String startDay, endDay, shopp_idd, store_admin_id,h,m, openingTime, clsoingTime;
    String clerks, clerksId;
    int iddd;

    public static AddAdminQDialog newInstance(int title) {
        AddAdminQDialog frag = new AddAdminQDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_qu_dialog_box, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivClose = view.findViewById(R.id.cancel_action);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvEndTime = view.findViewById(R.id.tvEndTime);

        spStartDay = view.findViewById(R.id.spinner1);
        spEndDay = view.findViewById(R.id.spinner2);
        spClerkList = view.findViewById(R.id.spClerkList);

        etQName = view.findViewById(R.id.etQname);
        etNoofQ = view.findViewById(R.id.etNoOfQ);
        bAddQ = view.findViewById(R.id.bAddQ);
        error = view.findViewById(R.id.error);
        setValue = view.findViewById(R.id.setValue);

        store_admin_id = Utilities.getString(getActivity(), "storeAdminId");

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sDOWEEKS, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spStartDay.setAdapter(adapter);

        spStartDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                startDay = spStartDay.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        String[] datos = getResources().getStringArray(R.array.eDOWEEKS);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, datos);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spEndDay.setAdapter(adaptador);

        spEndDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                endDay = spEndDay.getSelectedItem().toString();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        bAddQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clerksId = Utilities.getString(getActivity(), "ClerkId");
//                Toast.makeText(getActivity(), "onckick", Toast.LENGTH_SHORT).show();
                qName = etQName.getText().toString();
                noOfQ = etNoofQ.getText().toString();

                if (!qName.isEmpty()) {
                    error.setError(null);
                    if (!noOfQ.isEmpty() || noOfQ.equals("")) {
                        error.setError(null);
                        if (!TextUtils.isEmpty(startTime)) {
                            error.setError(null);
                            error.setText("");
                            if (!TextUtils.isEmpty(endTime)) {
                                error.setText("");
                                error.setError(null);

                                if (!startDay.equals("Opening Day")) {
                                    error.setError(null);
                                    error.setText("");
                                    if (!endDay.equals("Closing Day")) {
                                        error.setError(null);
                                        error.setText("");
                                        AddQApiApi();
                                    } else {
//                                        tvEndDay.setError("Field Required");
                                        error.setError("Closing Day required");
                                        error.setText("Closing Day required");

                                    }
                                } else {
                                    error.setError("Opening Day required");
                                    error.setText("Opening Day required");
//                                    tvStartDay.setError("Field Required");

                                }
                            } else {
                                error.setError("Closing Hour Required");
                                error.setText("Closing Hour Required");

                            }
                        } else {
                            error.setText("Opening Hour Required");
                            error.setError("Opening Hour Required");

                        }
                    } else {
                        etNoofQ.setError("Field Required");
                        error.setText("No. of Queue Required");
                        error.setError("");
                    }
                } else {
                    etQName.setError("Field Required");
                    error.setText("Queue Name Required");
                    error.setError("");

                }

            }
        });
        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if(hourOfDay<10){
                            h ="0"+String.valueOf(hourOfDay);
                        }else {

                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m ="0"+String.valueOf(minute);
                        }
                        else{
                            m=String.valueOf(minute);
                        }
                        startTime = h + ":" + m;
                        tvStartTime.setText(startTime);
                    }
                };

                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);

                // Whether show time in 24 hour format or not.
                boolean is24Hour = false;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), onTimeSetListener, hour, minute, is24Hour);
                timePickerDialog.setTitle("Select End Time.");

                timePickerDialog.show();
            }
        });
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String AM_PM;

//                            AM_PM = "AM";
//                            tvEndTime.setText(hourOfDay);
                        if(hourOfDay<10){
                            h ="0"+String.valueOf(hourOfDay);
                        }else {

                            h=String.valueOf(hourOfDay);
                        }
                        if(minute<10){
                            m ="0"+String.valueOf(minute);
                        }
                        else{
                            m=String.valueOf(minute);
                        }
//
                        endTime = h + ":" + m;
                        tvEndTime.setText(endTime);
                    }
                };

                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);

                // Whether show time in 24 hour format or not.
                boolean is24Hour = false;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), onTimeSetListener, hour, minute, is24Hour);
                timePickerDialog.setTitle("Select End Time.");

                timePickerDialog.show();
            }
        });
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
//        getClerkListApi();

        countriesAndregionApi();

    }


    private void countriesAndregionApi() {

        clerkListModels = new ArrayList<>();

        final StringRequest qmeRequest = new StringRequest(Request.Method.GET,"http://mtecsoft.com/qme/public/api/clerk_list", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status==200) {
                        final JSONArray objUser = object.getJSONArray("data");

                        for (int i = 0; i < objUser.length(); i++) {
//
                            JSONObject jsonObject = objUser.getJSONObject(i);

                            ClerkListModel clerkListModell = new ClerkListModel();

                            String name = jsonObject.getString("name");
                            int id = jsonObject.getInt("id");

                            clerkListModell.setClerkName(name);
                            clerkListModell.setClerkid(id);
                            clerkListModels.add(clerkListModell);


                            SpinnerAdapterClerks adapterrr = new SpinnerAdapterClerks(getContext(), android.R.layout.simple_spinner_dropdown_item, clerkListModels);
                            spClerkList.setAdapter(adapterrr);

                            spClerkList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//
                                     iddd = clerkListModels.get(position).getClerkid();


//                                    Utilities.makeToast(getContext(), region_code + region_id + region_name);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {


                                }
                            });


//
                        }
                    }
                    else{


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.hideProgressDialog();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

            }
        })
        {

//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                return params;
//
//            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
//
// @Override
// public Map<String, String> getHeaders() throws AuthFailureError {
// Map<String, String> params = new HashMap<>();
// params.put("Accept","application/json");
// return params;
// }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);


    }

    private void AddQApiApi() {


        shopp_idd = Utilities.getString(getActivity(), "admin_shopp_id");
        user_id = Utilities.getString(getActivity(), "id");
        store_admin_id = Utilities.getString(getActivity(), "storeAdminId");
//        shop_code = Utilities.getString(getActivity(), "shop_code");


        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.setIndeterminateDrawable(getActivity().getResources().getDrawable(R.drawable.anim, null));

        progressdialog.show();


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/qme/public/api/add_queue", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    if (status == 200) {


                        String message = object.getString("message");
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                        getDialog().cancel();

                        progressdialog.dismiss();


                    } else {
                        String message = object.getString("message");
                        new PrettyDialog(getActivity())
                                .setTitle(message)
                                .setIcon(R.drawable.pdlg_icon_info)
                                .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                .show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("store_admin_id", store_admin_id);
                params.put("shop", shopp_idd);
                params.put("queue_name", qName);
                params.put("number_of_persons", noOfQ);
                params.put("clerk", String.valueOf(iddd));
                params.put("status", "Active");
                params.put("opening_time", startTime);
                params.put("closing_time", endTime);
                params.put("opening_day", startDay);
                params.put("closing_day", endDay);

                return params;
            }
//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }

//    private void getClerkListApi() {
//
//        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
//        progressdialog.setIndeterminate(true);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.anim, null));
//
//        progressdialog.show();
//
//
//        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                try {
//                    clerkListModels = new ArrayList<>();
//                    clerkList = new ArrayList<String>();
//                    JSONObject object = new JSONObject(response);
//                    int status = object.getInt("success");
//                    if (status == 1) {
//                        final JSONArray objUser = object.getJSONArray("result");
////
//                        for (int i = 0; i < objUser.length(); i++) {
////
//                            JSONObject jsonObject = objUser.getJSONObject(i);
//
//                            String name = jsonObject.getString("name");
//                            String id = jsonObject.getString("id");
//                            Utilities.saveString(getActivity(), "ClerkId", id);
//
//                            clerkList.add(name);
//                            clerkListModels.add(new ClerkListModel(name, id));
//
//                            progressdialog.dismiss();
//
////
//                        }
////                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
////                                getActivity(), R.layout.spinner_item, clerkList
////                        );
//                        SpinnerAdapterClerks adapterrr = new SpinnerAdapterClerks(getContext(), android.R.layout.simple_spinner_dropdown_item, clerkList);
//                        spClerkList.setAdapter(adapterrr);
//
//
////                        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
////                        spClerkList.setAdapter(spinnerArrayAdapter);
////                        Collections.reverse(year_name_list);
//                        clerkList.add(0, "Select Clerk");
//                        spClerkList.setSelection(0);
//
//
//                    } else {
//                        if (status == 0) {
//                            String error = object.getString("message");
//                            Utilities.hideProgressDialog();
//                            new PrettyDialog(getActivity())
//                                    .setTitle(error + "for unassigned queue")
//                                    .setIcon(R.drawable.pdlg_icon_info)
//                                    .setIconTint(R.color.colorPrimary)
////                .setMessage("PrettyDialog Message")
//                                    .show();
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressdialog.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                progressdialog.dismiss();
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                if (getActivity() != null)
//                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("do", "clerk_list");
//                params.put("apikey", "mtechapi12345");
//                return params;
//            }
//
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                Map<String, String> params = new HashMap<>();
////                params.put("Accept", "application/json");
////                return params;
////            }
//        };
//
//        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
//                25000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
//    }
}